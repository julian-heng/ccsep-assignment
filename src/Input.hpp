#pragma once

#include <iostream>
#include <sstream>
#include <string>

int inputInt(std::string prompt, int min, int max);
double inputReal(std::string prompt, double min, double max);
std::string inputString(std::string prompt);