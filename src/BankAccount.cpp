#include <iostream>

#include "BankAccount.hpp"

BankAccount::BankAccount(std::string accName, int bsb, int accNum, double balance)
{
    this->accName = accName;
    this->bsb = bsb;
    this->accNum = accNum;
    this->balance = balance;
}

BankAccount::~BankAccount()
{
    std::cout << "Destroying Account at address: " << this << std::endl;
}

// Delete a transaction from list of transactions based on the number displayed on the screen
// TransNum will be -1 as index for vector starts at 0
void BankAccount::deleteTransaction(int transNum)
{
    delete this->transactions[transNum];
    this->transactions.erase(this->transactions.begin() + transNum);
}

// Used to properly deallocate memory for transaction objects
void BankAccount::destroyAllTransactions()
{
    for (Transaction* t : transactions)
    {
        delete t;
    }
    transactions.clear();
}

// Add a new transaction to list of transactions
void BankAccount::addTransaction(Transaction* inTransaction)
{
    this->transactions.push_back(inTransaction); // Push elements into back
}

// Obtain a list of all transactions to be displayed in account view
std::vector<Transaction*> BankAccount::getAllTransactions()
{
    return this->transactions;
}

// Obtain a single transaction by passing in the transaction number representing it on the screen
// Number is -1 to accomodate for vector starting at index 0
Transaction* BankAccount::getTransaction(int transNum)
{
    return this->transactions[transNum];
}

std::string BankAccount::toString() const
{
    std::stringstream ss; // Allows us to concatenate items into a string

    ss << this << " [+] " << this->accName << " | BSB: " << this->bsb << " | ACC_NO: " << this->accNum << " | $"
       << this->balance << std::flush;

    return ss.str();
}
