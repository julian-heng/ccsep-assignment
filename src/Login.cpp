#include "Login.hpp"

Login::Login(std::vector<User*> listUsers, HomeView* homeView)
{
    this->listUsers = listUsers;
    this->homeView = homeView;
}

Login::~Login()
{
}

bool Login::showLogin()
{
    int loginOption;
    int PAN = 00000000;
    std::string secureCode = "";
    std::string prompt;
    bool runningLogin = true;
    bool validCredentials = false;

    do
    {
        std::cout << "----------------------LOG-IN----------------------" << std::endl;

        prompt = "Please select a valid option:\n1) Enter PAN\n2) Enter Secure Code\n3) Login\n4) Sign-up\n0) Exit";
        loginOption = inputInt(prompt, 0, 4);

        switch (loginOption)
        {
            case 0:
                std::cout << "Exiting the program. Goodbye!" << std::endl;
                runningLogin = false;
                break;
            case 1:
                prompt = "Please enter a valid PAN: ";
                PAN = inputInt(prompt, 1, 999999999);
                break;
            case 2:
                prompt = "Please enter a valid secure code: ";
                secureCode = inputString(prompt);
                break;
            case 3:
                if ((PAN == 00000000) || (secureCode.empty()))
                {
                    std::cout << "You must enter a PAN or secure code value first" << std::endl;
                }
                else
                {
                    validCredentials = checkCredentials(PAN, secureCode);
                }
                break;
            case 4:
                addNewUser();
                break;
            default:
                std::cout << "You need to enter a valid option!" << std::endl;
                break;
        }
    } while (runningLogin);

    return validCredentials;
}

void Login::addNewUser()
{
    int newUserOption;
    int PAN = 00000000;
    std::string secureCode = "";
    std::string prompt;
    bool runningNewUser = true;

    std::cout << "----------------------SIGN-UP----------------------" << std::endl;

    do
    {
        prompt = "Please select a valid option:\n1) Enter PAN\n2) Create Secure Code\n3) Create User\n4) Back";
        newUserOption = inputInt(prompt, 0, 4);

        switch (newUserOption)
        {
            case 1:
                prompt = "Please enter a valid PAN: ";
                PAN = inputInt(prompt, 1, 999999999);
                break;
            case 2:
                prompt = "Please enter a valid secure code: ";
                secureCode = inputString(prompt);
                break;
            case 3:
                if ((PAN == 00000000) || (secureCode.empty()))
                {
                    std::cout << "You must enter a PAN or secure code value first to create a new user" << std::endl;
                }
                else
                {
                    listUsers.push_back(new User(PAN, secureCode));
                    std::cout << "You have successfully created a new user!" << std::endl;
                    runningNewUser = false;
                }
                break;
            case 4:
                runningNewUser = false;
                break;
            default:
                std::cout << "You need to enter a valid option" << std::endl;
                break;
        }
    } while (runningNewUser);
}

bool Login::checkCredentials(int PAN, std::string secureCode)
{
    bool validCredentials = false;

    // Check each user for matching credentials

    for (int i = 0; i < listUsers.size(); i++)
    {
        if ((listUsers[i]->getPAN() == PAN) && (listUsers[i]->getSecureCode().compare(secureCode) == 0))
        {
            validCredentials = true;
            std::cout << "Credentials match valid user" << std::endl;
            homeView->setCurrentUser(
                listUsers[i]);        // Configure the home view to obtain to the successfully logged in user
            homeView->showHomeView(); // Display home view interface specific to user
        }
    }

    if (!validCredentials)
    {
        std::cout << "Invalid username or password" << std::endl;
    }

    return validCredentials;
}
