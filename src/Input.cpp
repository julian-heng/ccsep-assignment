#include "Input.hpp"

#include <algorithm>
#include <limits>
#include <unistd.h>

int inputInt(std::string prompt, int min, int max)
{
    int value = 0;
    std::string in;

    std::stringstream error, outputPrompt;
    error << "ERROR: value must be between " << min << " and " << max << std::endl;
    outputPrompt << prompt;

    std::cout << prompt << std::endl;
    while (std::getline(std::cin, in))
    {
        if (! isatty(fileno(stdin)))
        {
            std::cout << in << std::endl;
        }

        try
        {
            value = std::stoi(in);
            if (value >= min && max >= value)
            {
                break;
            }
            else
            {
                std::cout << error.str();
            }
        }
        catch (...)
        {
            std::cout << error.str();
        }
    }

    return value;
}

double inputReal(std::string prompt, double min, double max)
{
    double value = min - 1.0;
    std::string in;

    std::stringstream error, outputPrompt;
    error << "ERROR: value must be between " << min << " and " << max << std::endl;
    outputPrompt << prompt;

    std::cout << prompt << std::endl;
    while (std::getline(std::cin, in))
    {
        if (! isatty(fileno(stdin)))
        {
            std::cout << in << std::endl;
        }

        try
        {
            value = std::stof(in);
            if (value >= min && max >= value)
            {
                break;
            }
            else
            {
                std::cout << error.str();
            }
        }
        catch (...)
        {
            std::cout << error.str();
        }
    }

    return value;
}

std::string inputString(std::string prompt)
{
    std::string in;
    std::string value = "";
    std::string error("ERROR: string entered must be NOT null\n");
    std::string outputPrompt("");

    outputPrompt = prompt;

    std::cout << outputPrompt << std::endl;
    while (std::getline(std::cin, in))
    {
        if (! isatty(fileno(stdin)))
        {
            std::cout << in << std::endl;
        }

        if (!in.empty())
        {
            value = in;
            break;
        }
        else
        {
            outputPrompt = error + prompt;
            std::cout << outputPrompt << std::endl;
        }
    }

    return value;
}
