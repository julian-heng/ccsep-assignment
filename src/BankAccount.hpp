#pragma once

#include "Transaction.hpp"
#include <sstream>
#include <vector>

class BankAccount
{
  private:
    std::string accName;
    int bsb;
    int accNum;
    double balance;
    std::vector<Transaction*> transactions;

  public:
    BankAccount(std::string accName, int bsb, int accNum, double balance);
    ~BankAccount();
    void deleteTransaction(int transNum);
    void destroyAllTransactions();
    void addTransaction(Transaction* inTransaction);
    std::vector<Transaction*> getAllTransactions();
    Transaction* getTransaction(int transNum);
    std::string toString() const;
};
