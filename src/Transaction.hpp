#pragma once

#include <iostream>
#include <sstream>
#include <string>

class Transaction
{
  private:
    std::string name;
    std::string location;
    std::string date;
    double amount;

  public:
    Transaction(std::string name, std::string location, std::string date, double amount);
    ~Transaction();
    std::string toString() const;
};
