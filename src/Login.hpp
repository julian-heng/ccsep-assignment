#pragma once

#include "HomeView.hpp"
#include "Input.hpp"
#include <iostream>
#include <sstream>
#include <stdio.h>
#include <string>

class Login
{
  private:
    HomeView* homeView;
    std::vector<User*> listUsers;

  public:
    Login(std::vector<User*> listUsers, HomeView* homeView);
    ~Login();
    bool showLogin();
    bool checkCredentials(int PAN, std::string secureCode);
    void addNewUser();
};