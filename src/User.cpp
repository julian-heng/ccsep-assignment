#include "User.hpp"
#include <iostream>

User::User(int PAN, std::string secureCode)
{
    this->PAN = PAN;
    this->secureCode = secureCode;
}

User::~User()
{
    destroyAllAccounts();
    std::cout << "Destroying User" << std::endl;
}

void User::deleteAccount(int accNum)
{
    delete this->bankAccounts[accNum];
    this->bankAccounts.erase(this->bankAccounts.begin() + accNum);
}

// Used to properly deallocate memory for all account objects
void User::destroyAllAccounts()
{
    for (BankAccount* b : bankAccounts)
    {
        delete b;
    }
    bankAccounts.clear();
}

int User::getPAN()
{
    return this->PAN;
}

std::vector<BankAccount*> User::getAllBankAccounts()
{
    return this->bankAccounts;
}

// Obtain a single bank account by passing in the account number representing the account on screen
// Number - 1 to accomodate for vector starting at index 0
BankAccount* User::getAccount(int accNum)
{
    return this->bankAccounts[accNum];
}

BankAccount* User::getCurrentAccount()
{
    return this->currentBankAccount;
}

void User::setCurrentAccount(BankAccount* inAccount)
{
    this->currentBankAccount = inAccount;
}

std::string User::getSecureCode()
{
    return this->secureCode;
}

// Add new bank account to list of accounts
void User::addAccount(BankAccount* inAccount)
{
    this->bankAccounts.push_back(inAccount); // Push elements into back
}
