#include "HomeView.hpp"

/*HomeView::HomeView(User* user, AccountView* accountView, std::vector<BankAccount*> bankAccounts)
{
    this->user;
    this->accountView;
    this->bankAccounts;
}*/

HomeView::HomeView()
{
}

HomeView::~HomeView()
{
}

void HomeView::showHomeView()
{
    std::cout << "----------------------HOME-VIEW----------------------\n" << std::endl;

    // Show other options
    int option, num;
    std::string prompt;
    bool running = true;
    bool validCredentials = false;

    do
    {
        prompt = "Please select a valid option:\n1) Show accounts\n2) Add account\n3) Delete account\n4) Delete all "
                 "accounts\n0) Back";
        option = inputInt(prompt, 0, 4);

        switch (option)
        {
            case 0:
                std::cout << "Going back!" << std::endl;
                running = false;
                break;
            case 1:
                std::cout << "Available accounts: \n" << std::endl;
                showAccounts();
                break;
            case 2:
                // Add account here
                this->user->addAccount(new BankAccount("NOT-WILL-STUDENT", 407199, 109333444, 0.03));
                break;
            case 3:
                // Delete account here
                // TODO: Delete in here before calling deleteAccount so that we can double free and change the list size
                num = inputInt("Enter an account number to delete.", 1, this->user->getAllBankAccounts().size());
                delete this->user->getAllBankAccounts()[num - 1];
                this->user->deleteAccount(num - 1);
                break;
            case 4:
                // Destroy all accounts
                this->user->destroyAllAccounts();
                break;
            default:
                std::cout << "You need to enter a valid option!" << std::endl;
                break;
        }
    } while (running);
    /*******************************************************/
}

void HomeView::showAccounts()
{
    std::vector<BankAccount*> bankAccounts = this->user->getAllBankAccounts();

    for (int i = 0; i < bankAccounts.size(); i++)
    {
        std::cout << i + 1 << ") " << bankAccounts[i]->toString() << std::endl;
    }
}

void HomeView::setCurrentUser(User* user)
{
    this->user = user;
}
