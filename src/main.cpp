#include <iostream>
#include <stdio.h>
#include <vector>

#include "BankAccount.hpp"
#include "Login.hpp"
#include "User.hpp"

using namespace std;

int main()
{
    // Declare list of users
    std::vector<User*> listUsers;
    Login* loginObj;
    HomeView* homeObj;

    // Initialise default user
    listUsers.push_back(new User(1234567, "password"));

    // Initialise default user bank accounts
    listUsers[0]->addAccount(new BankAccount("WILL-STUDENT", 407199, 109333444, 0.03));
    listUsers[0]->addAccount(new BankAccount("SAVINGS", 911000, 151111222, 7400.0));

    // Initialise default transactions to each account
    listUsers[0]->getAccount(1)->addTransaction(
        new Transaction("RTX3090 Purchase", "PERTH STADIUM", "20/1/1929", 696969.69));
    listUsers[0]->getAccount(1)->addTransaction(
        new Transaction("Tim's Warehouse - Apples", "GERTH STADIUM", "1/3/1555", 70.4));
    // listUsers[0]->getAccount(2)->addTransaction(new Transaction("Coles - Water Soup","DERTH
    // STADIUM","6/1/2049",992.0));

    std::cout << "WELCOME TO OUR SECURE BANKING APPLICATION" << std::endl;

    // Create and initialise home view
    homeObj = new HomeView();

    /* Testing process payment
    processPaymentObj->setCurrentUser(listUsers[0]);
    processPaymentObj->showProcessPaymentView();
    */

    // Create and initialise login
    loginObj = new Login(listUsers, homeObj);
    loginObj->showLogin();
    delete loginObj;

    // Clean up users
    for (User* u : listUsers)
    {
        delete u;
    }
    listUsers.clear();

    /*
    BankAccount* a = new BankAccount("WILL-STUDENT",407199,109333444,0.03);
    BankAccount* b = new BankAccount("WILL-STUDENT",407199,109333444,0.03);
    BankAccount* c = new BankAccount("WILL-STUDENT",407199,109333444,0.03);

    printf("%p\n", a);
    printf("%p\n", b);
    printf("%p\n", c);

    delete(a);
    delete(b);
    delete(a);

    BankAccount* d = new BankAccount("WILL-STUDENT",407199,109333444,0.03);
    BankAccount* e = new BankAccount("WILL-STUDENT",407199,109333444,0.03);
    BankAccount* f = new BankAccount("WILL-STUDENT",407199,109333444,0.03);

    printf("%p\n", d);
    printf("%p\n", e);
    printf("%p\n", f);*/

    return 0;
}
