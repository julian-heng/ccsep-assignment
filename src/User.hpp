#pragma once

#include "BankAccount.hpp"
#include <string>
#include <vector>

class User
{
  private:
    int PAN;
    std::string secureCode;
    std::vector<BankAccount*> bankAccounts;
    BankAccount* currentBankAccount;

  public:
    User(int PAN, std::string secureCode);
    ~User();
    void destroyAllAccounts();
    int getPAN();
    std::string getSecureCode();
    std::vector<BankAccount*> getAllBankAccounts();
    BankAccount* getAccount(int accNum);
    BankAccount* getCurrentAccount();
    void setCurrentAccount(BankAccount* inAccount);
    void addAccount(BankAccount* inAccount);
    void deleteAccount(int accNum);
};
