#include "Transaction.hpp"
#include <iostream>

Transaction::Transaction(std::string name, std::string location, std::string date, double amount)
{
    this->name = name;
    this->location = location;
    this->date = date;
    this->amount = amount;
}

Transaction::~Transaction()
{
    std::cout << "Destroying Transaction" << std::endl;
}

std::string Transaction::toString() const
{
    std::stringstream ss; // Allows us to concatenate items into a string

    ss << this << " [+] " << this->name << " | Location: " << this->location << " | " << this->date << " | $"
       << this->amount << std::endl;

    return ss.str();
}
