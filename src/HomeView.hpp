#pragma once

#include "BankAccount.hpp"
#include "Input.hpp"
#include "User.hpp"
#include <iostream>
#include <vector>

class HomeView
{
  private:
    User* user;
    /*AccountView* accountView;
    std::vector<BankAccount*> bankAccounts;*/
  public:
    // HomeView(User* user, AccountView* accountView, std::vector<BankAccount*> bankAccounts);
    HomeView();
    ~HomeView();
    void showHomeView();
    void showAccounts();
    void setCurrentUser(User* user);
};