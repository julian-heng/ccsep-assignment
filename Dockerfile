# A sample run a Flask program
# Guide: https://blog.bitsrc.io/a-guide-to-docker-multi-stage-builds-206e8f31aeb8
FROM ubuntu:18.04
MAINTAINER Your Name "youremail@domain.tld"

# Install required packages
RUN apt -y update && \
    apt -y install build-essential \
                   gdb \
                   valgrind \
                   cppcheck \
                   sudo \
                   less \
                   vim --no-install-recommends

# Create demo user
RUN useradd --create-home \
            --shell /bin/bash \
            --groups sudo \
            --user-group demo

# Set's demo password to "demo"
RUN echo "demo:demo" | chpasswd

USER demo
WORKDIR /home/demo

# Copy over demo source files
COPY --chown=demo:demo src /home/demo/src
COPY --chown=demo:demo research /home/demo/research
COPY --chown=demo:demo test /home/demo/test
