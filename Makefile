# A sample Makefile to use make command to build, test and run the program
# Guide: https://philpep.org/blog/a-makefile-for-your-dockerfiles/
APP=isec3004.csharper.assignment

all: build

build:
	(cd research && make clean)
	(cd src && make clean)
	docker build --rm --tag=$(APP) .
	docker image prune -f

run:
	docker run -it --rm $(APP)

clean:
	docker image rm $(APP)
	docker system prune -f

.PHONY: all test clean
